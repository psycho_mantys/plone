# syntax = docker/dockerfile:1
# pull official base image

#FROM python:2-slim-buster AS base
FROM python:2-slim AS base

# Env and Args

ENV PYTHONIOENCODING=UTF-8
ENV PIP_DISABLE_PIP_VERSION_CHECK=1

# locked version
ARG SETUPTOOLS_VERSION="38.7.0"
ENV SETUPTOOLS_VERSION=${SETUPTOOLS_VERSION}

# Postgres
ENV DATABASE_TYPE="postgresql"
ENV DATABASE_HOST="postgres-db-test"
ENV DATABASE_PORT="5432"
ENV DATABASE_NAME="postgres-db-name-test"
ENV DATABASE_USER="postgres-user-test"
ENV DATABASE_PASSWORD="CHANGEME"

# Plone
ENV PLONE_PORT=8080

ARG PLONE_USER=admin
ENV PLONE_USER=${PLONE_USER}

ARG PLONE_PASSWORD="CHANGEME"
ENV PLONE_PASSWORD=${PLONE_PASSWORD}

ARG BUILDOUT_VERBOSITY="-vv"
ENV BUILDOUT_VERBOSITY="${BUILDOUT_VERBOSITY}"

ARG PLONE_MAJOR=5.0
ENV PLONE_MAJOR="${PLONE_MAJOR}"

ARG PLONE_VERSION=5.0.8
ENV PLONE_VERSION="${PLONE_VERSION}"

ARG PLONE_MD5=246788240851f48bc2f84289a3dc6480
ENV PLONE_MD5="${PLONE_MD5}"

LABEL plone=$PLONE_VERSION \
    os="debian" \
    os.version="8" \
    name="Plone 5" \
    description="Plone image, based on Unified Installer(With ldap, relstorage e another addons)" \
    maintainer="Psycho Mantys"

ARG BUILD_PACKAGES="libldap2-dev libpq-dev wget sudo python-setuptools python-dev build-essential libssl-dev libxml2-dev libxslt1-dev libbz2-dev libjpeg62-turbo-dev libtiff5-dev libopenjp2-7-dev libsasl2-dev libyaml-dev libimagequant-dev libfreetype6-dev liblcms2-dev libwebp-dev default-libmysqlclient-dev"
ENV BUILD_PACKAGES=${BUILD_PACKAGES}

ARG RUNTIME_PACKAGES="gosu libldap-2.4-2 netcat libpq-dev libxml2 libxslt1.1 libjpeg62 rsync lynx wv libtiff5 libopenjp2-7 poppler-utils libsasl2-2 libyaml-0-2 libimagequant0 libfreetype6 liblcms2-2 webp libmariadb3"
ENV RUNTIME_PACKAGES=${RUNTIME_PACKAGES}

RUN useradd --system -m -d /plone -U -u 1999 plone \
 && mkdir -p /data/filestorage /data/blobstorage \
 && chown -R plone:plone /data

FROM base AS buildout

RUN apt-get update \
 && apt-get install -y --no-install-recommends ${BUILD_PACKAGES} \
 && wget -O Plone.tgz https://launchpad.net/plone/$PLONE_MAJOR/$PLONE_VERSION/+download/Plone-$PLONE_VERSION-UnifiedInstaller.tgz \
 && echo "${PLONE_MD5} Plone.tgz" | md5sum -c - \
 && tar -xzf Plone.tgz \
 && ./Plone-$PLONE_VERSION-UnifiedInstaller/install.sh \
      --password=admin \
      --daemon-user=plone \
      --owner=plone \
      --group=plone \
      --target=/plone \
      --instance=instance \
      --var=/data \
      none

COPY plone/plone.cfg /plone/instance/site.cfg
RUN sed -i s";PLONE_USER:PLONE_PASSWORD;${PLONE_USER}:${PLONE_PASSWORD};" "/plone/instance/site.cfg"

# BUGFIX: TypeError: 'Version' object has no attribute '__getitem__'
# bin/pip install setuptools==38.7.0
RUN --mount=type=cache,mode=0755,target=/pip_cache --mount=type=cache,mode=0755,target=/plone/buildout-cache/downloads/ \
 cd /plone/instance \
 && mkdir -p "/plone/buildout-cache/downloads/" \
 && chown plone.plone -Rv "/plone/buildout-cache/downloads/" \
 && bin/pip install --trusted-host pypi.python.org --trusted-host pypi.org --trusted-host files.pythonhosted.org --cache-dir /pip_cache setuptools==${SETUPTOOLS_VERSION} \
 && sed -i 's/parts =/parts =\n    zeoserver/g' buildout.cfg \
 && echo '\n[zeoserver]\n<= zeoserver_base\nrecipe = plone.recipe.zeoserver' >> buildout.cfg \
 && sudo -u plone bin/buildout -c site.cfg ${BUILDOUT_VERBOSITY} \
 && rm -rf /Plone* \
 && rm -rf /plone/Plone-docs \
 && find /plone \( -type f -a -name '*.pyc' -o -name '*.pyo' \) -exec rm -rf '{}' +
# && find /data  -not -user plone -exec chown plone:plone {} \+ \
# && find /plone -not -user plone -exec chown plone:plone {} \+ \
# && rm -rf /plone/buildout-cache/downloads/* \

FROM base

COPY --from=buildout --chown=plone:plone /plone /plone

WORKDIR /plone/instance

RUN apt-get update \
 && SUDO_FORCE_REMOVE=yes apt-get remove --purge -y ${BUILD_PACKAGES} \
 && apt-get autoremove -y \
 && rm -rf /usr/share/man \
 && apt-get install -y --no-install-recommends ${RUNTIME_PACKAGES} \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* \
 && mv parts/instance/etc/zope.conf parts/instance/etc/zope.conf.orig \
 && find /data  -not -user plone -exec chown plone:plone {} \+ \
 && find /plone -not -user plone -exec chown plone:plone {} \+

VOLUME /data

COPY plone/docker-entrypoint.sh /
COPY wait-for /

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["start"]
#CMD ["sleep", "2h"]

HEALTHCHECK --interval=90s --timeout=5s --start-period=1m \
  CMD /docker-entrypoint.sh healthcheck

#EXPOSE ${PLONE_PORT}

