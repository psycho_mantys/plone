#!/bin/bash

do_gosu(){
	user="$1"
	shift 1

	is_exec="false"
	if [ "$1" = "exec" ] ; then
		is_exec="true"
		shift 1
	fi

	if [ "$(id -u)" = "0" ] ; then
		if [ "${is_exec}" = "true" ] ; then
			exec gosu "${user}" "$@"
		else
			gosu "${user}" "$@"
		fi
	else
		if [ "${is_exec}" = "true" ] ; then
			exec "$@"
		else
			eval "$@"
		fi
	fi
}

set -e

COMMANDS="debug help logtail show stop adduser fg kill quit run wait console foreground logreopen reload shell status"
START="start restart zeoserver"
CMD="bin/instance"

CONF_FILES=(
	"parts/instance/etc/zope.conf"
)

replace_conf(){
	for arg in "$@" ; do
		local key="$( cut -f1 -d'=' <<<"${arg}" )"
		local value="$( cut -f2- -d'=' <<<"${arg}" )"
		for f in "${CONF_FILES[@]}" ; do
			sed -i s";${key};${value};" "${f}"
		done
	done
}

parse_env(){
	if [ -r "$1" ] ; then
		while IFS="=" read key value  ; do
			export "${key}=${value}"
		done<<<"$( egrep '^[^#]+=.*' "$1" )"
	fi
}

bootstrap_conf(){
	cp 'parts/instance/etc/zope.conf.orig' 'parts/instance/etc/zope.conf'

	# Fixing permissions for external /data volumes
	mkdir -p /data/blobstorage /data/cache /data/filestorage /data/instance /data/log /data/zeoserver
	mkdir -p /plone/instance/src

	if [ "x${PLONE_DO_CHOWN}" = "xtrue" ] ; then
		find /data /plone -not -user plone | xargs -n 20 chown plone:plone
		#find /plone -not -user plone -exec chown plone:plone {} \+
	fi

	replace_conf \
		PLONE_PORT="${PLONE_PORT}" \
		PLONE_USER="${PLONE_USER}" \
		PLONE_PASSWORD="${PLONE_PASSWORD}" \
		DATABASE_TYPE="${DATABASE_TYPE}" \
		DATABASE_HOST="${DATABASE_HOST}" \
		DATABASE_PORT="${DATABASE_PORT}" \
		DATABASE_NAME="${DATABASE_NAME}" \
		DATABASE_USER="${DATABASE_USER}" \
		DATABASE_PASSWORD="${DATABASE_PASSWORD}"

	do_gosu plone bin/instance adduser "${PLONE_USER}" "${PLONE_PASSWORD}"
}

parse_env '/env.sh'
parse_env '/run/secrets/env.sh'

if [[ "$1" == "zeo"* ]]; then
	CMD="bin/zeoserver"
fi

if [[ "${START}" == *"$1"* ]]; then
	/wait-for "${DATABASE_HOST}:${DATABASE_PORT}" -- echo DB "${DATABASE_HOST}:${DATABASE_PORT}" started
	bootstrap_conf
	do_gosu plone "${CMD}" start
	do_gosu plone exec "${CMD}" logtail
elif [[ "${COMMANDS}" == *"$1"* ]]; then
	bootstrap_conf
	do_gosu plone exec bin/instance "$@"
elif [[ "healthcheck" == "$1" ]]; then
	nc -z -w5 127.0.0.1 "${PLONE_PORT}" || exit 1
	exit 0
fi

exec "$@"

