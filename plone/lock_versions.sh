#!/bin/bash

grep eggs | \
   sed -r 's#.*eggs/(.*)-py2.[0-9].*#\1#g' | \
   sed -r 's#-# = #g' | \
   sed -r 's#_#-#g' | \
   grep -E ' = [0-9\.]' | \
   xargs -0 echo -e "[versions]\n" | \
   sed -r 's#^\s+##g'

